<?php



// exercise 1
echo 'EXO 1';
echo '<br>';
$word = "c'est ";
$word2 = "vie.";
$word3 = "Simplon";
$word4 = "la";

echo $word3 . ' ' . $word . ' ' . $word4 . ' ' . $word2;

// exercise 2

echo '<br>';
echo '<br>';
echo 'EXO 2';
echo '<br>';
$variable1 = 12;
$variable2 = 4;
echo $variable1 / $variable2;

// exercise 3
echo '<br>';
echo '<br>';
echo 'EXO 3';
echo '<br>';
$var = "clement";
$var1 = "extrapolation";

echo strlen($var . $var1);

// exercise 4
echo '<br>';
echo '<br>';
echo 'EXO 4';
echo '<br>';
$maitrise_du_code = 1000;
if ($maitrise_du_code >= 1000) {
    echo 'Je maitrise tellement le code maintenant!';
}

// exercise 5
echo '<br>';
echo '<br>';
echo 'EXO 5';
echo '<br>';
$chiffre_fetiche_sandrine = 7;
$chiffre_fetiche_xavier = 130;
$chiffre_fetiche_andre = 8;
//inversement de la valeur des variables
$chiffre_fetiche_sandrine = $chiffre_fetiche_sandrine + $chiffre_fetiche_xavier;
$chiffre_fetiche_xavier = $chiffre_fetiche_sandrine - $chiffre_fetiche_xavier;
$chiffre_fetiche_sandrine = $chiffre_fetiche_sandrine - $chiffre_fetiche_xavier;
// echo $chiffre_fetiche_sandrine;
// echo '<br>';
// echo $chiffre_fetiche_xavier;

echo '<br>';
$result = $chiffre_fetiche_sandrine - ($chiffre_fetiche_xavier . $chiffre_fetiche_andre);
if ($result < 50) {
    echo $result;
}

// exercise 6
echo '<br>';
echo '<br>';
echo 'EXO 6';
echo '<br>';
$compte = -100;
if ($compte > 0) {
    echo 'Bravo vous êtes un bon gestionnaire!';
} else {
    echo 'Vous faites vraiment n\'importe quoi avec votre argent!';
}

// exercise 7
echo '<br>';
echo '<br>';
echo 'EXO 7';
echo '<br>';
$vent = 20;
$houle = 10;
$cadence_vague = 2;

if ($vent > 30 && $houle < 20 && $cadence_vague < 10 || $vent < 30 && $houle < 30 && $cadence_vague < 8) {
    echo "On peut aller surfer";
} else {
    echo "Le condition ne sont pas bonnes pour aller surfer";
}

// exercise 8
echo '<br>';
echo '<br>';
echo 'EXO 8';
echo '<br>';
$nombre_1 = 88;
$nombre_2 = 7;
$nombre_3 = 23;
$nombre_4 = 5;
$nombre_5 = 45;
$nombre_6 = 12;

$sum_paires = 0;
$sum_unpaires = 0;
//VERSION COURTE
$num_tab = [$nombre_1, $nombre_2, $nombre_3, $nombre_4, $nombre_5, $nombre_6];

foreach ($num_tab as $num) {
    if ($num % 2 == 0) {
        $sum_paires += $num;
    } else {
        $sum_unpaires += $num;
    }
}


//VERSION LONGUE

// if ($nombre_1 % 2 == 0) {
//     $sum_paires += $nombre_1;
// }else{
//     $sum_unpaires += $nombre_1;
// }
// if ($nombre_2 % 2 == 0) {
//     $sum_paires += $nombre_2;
// }else{
//     $sum_unpaires += $nombre_2;
// }
// if ($nombre_3 % 2 == 0) {
//     $sum_paires += $nombre_3;
// }else{
//     $sum_unpaires += $nombre_3;
// }
// if ($nombre_4 % 2 == 0) {
//     $sum_paires += $nombre_4;
// }else{
//     $sum_unpaires += $nombre_4;
// }
// if ($nombre_5 % 2 == 0) {
//     $sum_paires += $nombre_5;
// }else{
//     $sum_unpaires += $nombre_5;
// }
// if ($nombre_6 % 2 == 0) {
//     $sum_paires += $nombre_6;
// }else{
//     $sum_unpaires += $nombre_6;
// }

echo $sum_paires;
echo '<br>';
echo $sum_unpaires;
